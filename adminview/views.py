from django.shortcuts import render
from django.http import HttpResponse
from .models import ClassList, Student, LibraryBook
from django.shortcuts import redirect
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def dashboard(response):
	daftarKelas = ClassList.objects.count()
	daftarMurid = Student.objects.count()
	daftarBuku = LibraryBook.objects.count()
	daftarBukuDipinjam = LibraryBook.objects.exclude(borrowedBy='Tidak sedang dipinjam').count();
	return render(response, "adminview/dashboard.html",{"jumlahKelas":daftarKelas,"jumlahMurid":daftarMurid,"daftarBuku":daftarBuku,"daftarBukuDipinjam":daftarBukuDipinjam})

@login_required
def datakelas(response):
	daftarKelas = ClassList.objects.all()

	if response.method == "POST":
		print("test")
		if response.POST.get("saveNewClass"):
			newClassName = response.POST.get("newClassName")
			newClassDescription = response.POST.get("newClassDescription")
			newClassItem = ClassList(name=newClassName,description=newClassDescription)
			newClassItem.save()
			response.user.classlist.add(newClassItem)

	return render(response, "adminview/datakelas.html",{"daftarKelas":daftarKelas})

def hapuskelas(response,id):
	kelasMana = ClassList.objects.get(id=id)
	kelasMana.delete()
	return redirect('/adminview/datakelas')

def ubahdatakelas(response,id):
	sebuahKelas = ClassList.objects.get(id=id)
	return render(response, "adminview/ubahdatasebuahkelas.html",{"sebuahKelas":sebuahKelas})
def ubahkelas(response,id):
	sebuahKelas = ClassList.objects.get(id=id)
	sebuahKelas.name = response.POST.get("newClassName")
	sebuahKelas.description = response.POST.get("newClassDescription")
	sebuahKelas.save()
	return redirect('/adminview/datakelas')

@login_required
def datamurid(response):
	daftarMurid = Student.objects.all()

	if response.method == "POST":
		print("Murid baru sedang dimasukkan..")
		if response.POST.get("saveNewStudent"):
			newStudentName = response.POST.get("newStudentsName")
			newStudentsNIM = response.POST.get("newStudentsNIM")
			newStudentsDOB = response.POST.get("newStudentsDOB")
			newStudentGender = response.POST.get("newStudentsGender")
			newStudentsAddress = response.POST.get("newStudentsAddress")
			newStudentsFathersName = response.POST.get("newStudentsFathersName")
			newStudentsMothersName = response.POST.get("newStudentsMothersName")

			upload = response.FILES['newStudentsPhoto']
			fss = FileSystemStorage()
			file = fss.save(upload.name, upload)
			file_url = fss.url(file)

			newStudentsPhoto = upload.name
			newStudentItem = Student(name=newStudentName,registrationNumber=newStudentsNIM,dateOfBirth=newStudentsDOB,gender=newStudentGender,address=newStudentsAddress,fathersName=newStudentsFathersName,mothersName=newStudentsMothersName,photo=newStudentsPhoto)
			newStudentItem.save()


	return render(response, "adminview/datamurid.html",{"daftarMurid":daftarMurid})
@login_required
def dataseorangmurid(response,id):
	seorangMurid = Student.objects.get(id=id)
	return render(response, "adminview/dataseorangmurid.html",{"seorangMurid":seorangMurid})
	
def hapusmurid(response,id):
	muridMana = Student.objects.get(id=id)
	muridMana.delete()
	return redirect('/adminview/datamurid')

def ubahdatamurid(response,id):
	seorangMurid = Student.objects.get(id=id)
	return render(response, "adminview/ubahdataseorangmurid.html",{"seorangMurid":seorangMurid})
def ubahmurid(response,id):
	seorangMurid = Student.objects.get(id=id)
	seorangMurid.name = response.POST.get("newStudentsName")
	seorangMurid.registrationNumber = response.POST.get("newStudentsNIM")
	seorangMurid.dateOfBirth = response.POST.get("newStudentsDOB")
	seorangMurid.gender = response.POST.get("newStudentsGender")
	seorangMurid.address = response.POST.get("newStudentsAddress")
	seorangMurid.fathersName = response.POST.get("newStudentsFathersName")
	seorangMurid.mothersName = response.POST.get("newStudentsMothersName")

	upload = response.FILES['newStudentsPhoto']
	fss = FileSystemStorage()
	file = fss.save(upload.name, upload)
	file_url = fss.url(file)
	seorangMurid.photo = upload.name
	seorangMurid.save()
	return redirect('/adminview/datamurid')

@login_required
def dataPerpustakaan(response):
	daftarBuku = LibraryBook.objects.all()
	daftarMurid = Student.objects.all()

	if response.method == "POST":
		print("test")
		if response.POST.get("saveNewBook"):
			newBookName = response.POST.get("newBookName")
			newBookRegistrationNumber = response.POST.get("newBookRegistrationNumber")
			newBookBorrowedBy = response.POST.get("newBookBorrowedBy")
			newBookBorrowedFrom = response.POST.get("newBookBorrowedFrom")
			newBookBorrowedTo = response.POST.get("newBookBorrowedTo")

			upload = response.FILES['newBookPhoto']
			fss = FileSystemStorage()
			file = fss.save(upload.name, upload)
			file_url = fss.url(file)
			newBookPhoto = upload.name
			newBookItem = LibraryBook(name=newBookName,registrationNumber=newBookRegistrationNumber,borrowedBy=newBookBorrowedBy,borrowedFrom=newBookBorrowedFrom,borrowedTo=newBookBorrowedTo, photo=newBookPhoto )
			newBookItem.save()

	return render(response, "adminview/dataperpustakaan.html",{"daftarBuku":daftarBuku,"daftarMurid":daftarMurid})

def hapusBuku(response,id):
	sebuahBuku = LibraryBook.objects.get(id=id)
	sebuahBuku.delete()
	return redirect('/adminview/dataperpustakaan')

def ubahDataBuku(response,id):
	sebuahBuku = LibraryBook.objects.get(id=id)
	return render(response, "adminview/ubahdatasebuahbuku.html",{"sebuahBuku":sebuahBuku})

def ubahBuku(response,id):
	sebuahBuku = LibraryBook.objects.get(id=id)
	sebuahBuku.name = response.POST.get("newBookName")
	sebuahBuku.registrationNumber = response.POST.get("newBookRegistrationNumber")
	sebuahBuku.borrowedBy = response.POST.get("newBookBorrowedBy")
	sebuahBuku.borrowedFrom = response.POST.get("newBookBorrowedFrom")
	sebuahBuku.borrowedTo = response.POST.get("newBookBorrowedTo")
	upload = response.FILES['newBookPhoto']
	fss = FileSystemStorage()
	file = fss.save(upload.name, upload)
	file_url = fss.url(file)
	sebuahBuku.photo = upload.name
	sebuahBuku.save()
	return redirect('/adminview/dataperpustakaan')












from .forms import SignUpForm
 
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  
            # load the profile instance created by the signal
            user.save()
            raw_password = form.cleaned_data.get('password1')
 
            # login user after signing up
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
 
            # redirect user to home page
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				return redirect("/adminview/dashboard")
	form = AuthenticationForm()
	return render(request=request, template_name="adminview/login.html", context={"login_form":form})

def logout_request(request):
	logout(request)
	return redirect("/adminview/login")
from django.contrib import admin
from .models import ClassList, Student

# Register your models here.
admin.site.register(ClassList)
admin.site.register(Student)
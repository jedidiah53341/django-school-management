from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ClassList(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="classlist", null=True) 
	name = models.CharField(max_length=200)
	description = models.CharField(max_length=200)

class Student(models.Model):
	name = models.CharField(max_length=200)
	registrationNumber = models.PositiveBigIntegerField()
	dateOfBirth = models.DateField()
	gender = models.CharField(max_length=20)
	address = models.CharField(max_length=200)
	fathersName = models.CharField(max_length=200)
	mothersName = models.CharField(max_length=200)
	photo = models.ImageField(upload_to='blog/%Y/%m/%d')

class LibraryBook(models.Model):
	name = models.CharField(max_length=200)
	registrationNumber = models.PositiveBigIntegerField()
	borrowedBy = models.CharField(max_length=200,null=True,)
	borrowedFrom= models.DateField(null=True,)
	borrowedTo = models.DateField(null=True,)
	photo = models.ImageField(upload_to='blog/%Y/%m/%d')
from django.urls import path

from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
path("dashboard",views.dashboard, name="Admin's Dashboard"),

path("datakelas",views.datakelas, name="Data Kelas"),
path("hapuskelas/<int:id>",views.hapuskelas, name="Hapus Kelas"),
path("ubahdatakelas/<int:id>",views.ubahdatakelas, name="Ubah Data Kelas"),
path("ubahkelas/<int:id>",views.ubahkelas, name="Ubah Kelas"),

path("datamurid",views.datamurid, name="Data Murid"),
path("datamurid/<int:id>",views.dataseorangmurid, name="Data Seorang Murid"),
path("hapusmurid/<int:id>",views.hapusmurid, name="Hapus Murid"),
path("ubahdatamurid/<int:id>",views.ubahdatamurid, name="Ubah Data Murid"),
path("ubahmurid/<int:id>",views.ubahmurid, name="Ubah Murid"),

path("dataperpustakaan",views.dataPerpustakaan, name="Data Perpustakaan"),
path("hapusbuku/<int:id>",views.hapusBuku, name="Hapus Buku"),
path("ubahdatabuku/<int:id>",views.ubahDataBuku, name="Ubah Data Buku"),
path("ubahbuku/<int:id>",views.ubahBuku, name="Ubah Buku"),

path('signup/', views.signup, name='signup'), #added
path("login/", views.login_request, name="login"),
path("logout", views.logout_request, name= "logout"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
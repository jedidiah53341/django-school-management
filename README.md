# django-school-management

A django dashboard to manage students, classes, and library books. It has CRUDs, Login-Signup, and logged-in-only pages. Just a small project to get myself familiar with Django on a beginner level.
